const API_KEY = "31d0317c08b24cbb188acd1f450d7db8b5cd10c4695416b92f25cc665ab91576";

const tickersHandlers = new Map(); //обработчики событий

const socket = new WebSocket(`wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`);

const AGGREGATE_INDEX = "5";

socket.addEventListener('message', e => {

    const {TYPE: type, FROMSYMBOL: currency, PRICE: newPrice } = JSON.parse(e.data);

    if (type !== AGGREGATE_INDEX || newPrice === undefined) {
        return;
    }

    const handlers = tickersHandlers.get(currency) ?? [];
    handlers.forEach( fn => fn(newPrice)); 

});

function sendToWebSocket(message) {
    const stringifiedMessage = JSON.stringify(message);

    if (socket.readyState === WebSocket.OPEN) {
        socket.send(stringifiedMessage);
        return;
    }

    // жду, когда сокет откроется и посылаю сообщение
    socket.addEventListener('open', () => {
        socket.send(stringifiedMessage);
    }, { once: true });
}

function subscribeToTickerOnWs(ticker) {
    sendToWebSocket({
        action: "SubAdd",
        subs: [`5~CCCAGG~${ticker}~USD`]
    });
}

function unsubscribeFromTickerOnWs(ticker) {
    sendToWebSocket( {
        action: "SubRemove",
        subs: [`5~CCCAGG~${ticker}~USD`]
    } );
}

// нужно вызвать всех, кто раньше подписался на изменение тикера и новую ф-ию callback
export const subscribeToTicker = (ticker, callback) => {
    const subscribers = tickersHandlers.get(ticker) || [];
    tickersHandlers.set(ticker, [...subscribers, callback]);

    subscribeToTickerOnWs(ticker);
}

export const unsubscribeFromTicker = (ticker) => {
    tickersHandlers.delete(ticker);

    unsubscribeFromTickerOnWs(ticker);
}